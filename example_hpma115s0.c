/*
 *  PIC code for HPMA115S0 PM2.5/PM10 airborne particle detector
 */


// PIC setup
#include <16F1827.h>
#define PC_SERIAL PIN_A1

#fuses INTRC_IO,NOWDT,NOPROTECT,NOLVP,MCLR
#use delay(clock=8000000)
// Start serial connection to computer
#use rs232(baud=9600, xmit=PC_SERIAL, STREAM=PC)

#include <stdint.h>
#include <stdbool.h>
#include "hpma115s0.c"


void main() {
  fputs("Starting up", PC);

  initPMSensor();

  startMeasurement();           // Start fan

  uint16_t measurement;         // Variable to store combined PM2.5 and PM10 measurements
  uint8_t pm2_5, pm10;          // Variables for measurements split apart

  while(1) {
      measurement = measureParticles();
      pm10 = measurement >> 8;
      pm2_5 = measurement & 0xFF;
      fprintf(PC, "\r\nPM10: %u ", pm10);
      fprintf(PC, "PM2.5: %u", pm2_5);
      delay_ms(500);
  }
}
