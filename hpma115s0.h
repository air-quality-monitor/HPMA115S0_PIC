/*
 *  Library for HPMA115S0 PM2.5/PM10 airborne particle detector
 *  Header file for function declarations and constants
 */


// Pin numbers
#define HPM_TX PIN_B0
#define HPM_RX PIN_B2
#define HPM_ENABLE PIN_B3


// Acknowledge and negative acknowledge constants
#define HPM_ACK 0xA5            // Repeated twice
#define HPM_NACK 0x96           // Repeated twice


// Function prototypes
void initPMSensor(void);
void disableAutoSend(void);
void startMeasurement(void);
void stopMeasurement(void);
uint16_t measureParticles(void);
void sendCmd(uint8_t * cmd, uint8_t cmd_len);
bool checkAck(void);
bool checkChecksum(uint8_t * buf);
