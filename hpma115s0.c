/*
 *  Library for HPMA115S0 PM2.5/PM10 airborne particle detector
 *  Source file for function definitions
 */


#include "hpma115s0.h"          // Include header file for function declarations and constants


// Start serial connection to particle monitor
#use rs232(baud=9600, xmit=HPM_RX, rcv=HPM_TX, STREAM=HPM, timeout=1000)


void initPMSensor(void) {
  /*
   *  Re-power the sensor and disable auto-send of measurements
   *  This assumes you've already started a serial with stream HPM
   */
  output_bit(HPM_ENABLE, 0);
  delay_ms(100);
  output_bit(HPM_ENABLE, 1);

  delay_ms(100);

  disableAutoSend();
}


void disableAutoSend(void) {
  /*
   *  Use to fetch measurements when needed instead of the sensor sending new ones
   */
  uint8_t cmd[] = {0x68, 0x01, 0x20, 0x77};
  sendCmd(cmd, 4);
}


void startMeasurement(void) {
  uint8_t cmd[4] = {0x68, 0x01, 0x01, 0x96};
  delay_ms(5000);                  // Allow airflow for a bit
  sendCmd(cmd, 4);
}


void stopMeasurement(void) {
  uint8_t cmd[] = {0x68, 0x01, 0x02, 0x95};
  sendCmd(cmd, 4);
}


uint16_t measureParticles(void) {
  /*  Read PM2.5 and PM10 particle concentrations
   *  ACK response:
   *  HEAD LEN  CMD  DATA[4] CS
   *  0x40 0x05 0x04 04 00 30 00 31 56
   *  NACK response:
   *  0x96 0x96
   */

  // Send measure particles command
  uint8_t cmd[] = {0x68, 0x01, 0x04, 0x93};

  // Again, should be unnecessary on 6F819:
  // Flush input buffer (otherwise error will propagate)
  //while (hpm_serial.available()) hpm_serial.read();

  for(uint8_t i=0; i<4; i++) {
    //hpm_serial.write(cmd[i]);
    fputc(cmd[i], HPM);
  }

  // Read response (check first 2 for NACK)
  uint8_t buf[8];
  buf[0] = fgetc(HPM);
  buf[1] = fgetc(HPM);

  if (buf[0] == 0x96 || buf[1] == 0x96) {
    fputs("Measurement NACK received", PC);
    return 0xFF00;
  }

  // Read the rest
  //for (uint8_t j=2; j<8; j++) {
  //  buf[i] = fgetc(HPM);
  //}

  // Read the rest
  // For loop seems to be too slow?
  buf[2] = fgetc(HPM);
  buf[3] = fgetc(HPM);
  buf[4] = fgetc(HPM);
  buf[5] = fgetc(HPM);
  buf[6] = fgetc(HPM);
  buf[7] = fgetc(HPM);

  // Make sure checksum is correct
  if (!checkChecksum(buf)) {
    fputs("Checksum mismatch!", PC);
    for(uint8_t k=0; k<8; k++) {
      fprintf(PC, "%X ", buf[k]);
    }
    fputs("\r\n", PC);
    // Recursion not permitted, otherwise would do this:
    //return measureParticles();  // Try again if not correct
    return 0xFF00; // Instead, return FF00 if checksum incorrect - will not show on graph
  }

  // Print result if checksum correct
  else {
    uint16_t pm2_5=0, pm10=0;
    uint16_t result;
    pm2_5 = buf[3]*256 + buf[4];
    pm10 = buf[5]*256 + buf[6];

    //fprintf(PC, "PM2.5: %LU ug/m3\r\n", pm2_5);
    //fprintf(PC, "PM10: %LU ug/m3\r\n\r\n", pm10);

    // Combine PM10 and PM2.5 measurements into one 16-bit int
    // Use 8 LSBs of each result (higher not needed)
    // PM10 takes up first 8 bits, PM2.5 second 8
    result = pm10 << 8 | (pm2_5 & 0x00FF);

    // Catch overflows of the 8-bit values
    if (pm10 > 255) result |= 0xFF00;   // Set PM10 byte high if over 255
    if (pm2_5 > 255) result |= 0x00FF;  // Set PM2.5 byte high if over 255

    return result;
  }
}


void sendCmd(uint8_t * cmd, uint8_t cmd_len) {
  /*
   *  Write command byte-by-byte
   *  Retry if NACK
   */
  do {
    for(uint8_t i=0; i<cmd_len; i++) {
      fputc(cmd[i], HPM);
    }
  } while (!checkAck());
}


bool checkAck(void) {
  /*
   *  Return whether sensor's response is ACK or NACK
   */

  uint8_t buf[2];

  buf[0] = fgetc(HPM);          // Read two bytes
  buf[1] = fgetc(HPM);

  // Print failed results over serial for now
  if (buf[0] == HPM_ACK && buf[1] == HPM_ACK) {
    fputs("\r\nRX: ACK", PC);
    return true;
  }
  else if (buf[0] == HPM_NACK && buf[1] == HPM_NACK) {
    fputs("\r\nRX: NACK", PC);
    return false;
  }
  else {
    fprintf(PC, "\r\nRX: %X%X", buf[0], buf[1]);
    return false;
  }
}


bool checkChecksum(uint8_t * buf) {
  /*
   *  Confirm the checksum of the buffer
   */

  uint8_t head=buf[0], len=buf[1], cmd=buf[2], df1=buf[3],
    df2=buf[4], df3=buf[5], df4=buf[6], cs=buf[7], checksum;
  uint16_t sum;

  sum = (uint16_t) head + len + cmd + df1 + df2 + df3 + df4;
  checksum = (65536 - sum) % 256;

  //fprintf(PC, "%X %X %X %X %X %X %X\r\n", head, len, cmd, df1, df2, df3, df4);
  //fprintf(PC, "Sum: %LX CSum: %X, cs: %X\r\n", sum, checksum, cs);

  return checksum == cs;
}
